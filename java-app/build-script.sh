#!/bin/bash
echo "***************"
echo "***building***"
echo "***************"
docker build -f Dockerfile-java-build -t gradle-build .
docker run --rm -it -v $PWD/build:/app/build -w /app  gradle-build ./gradlew build