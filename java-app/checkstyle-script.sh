#!/bin/bash
echo "***************"
echo "***checkstyle***"
echo "***************"
docker build -f java-app/Dockerfile-java-build -t gradle-build .
docker run --rm -v $PWD/java-app/build/reports/pmd:/app/build/reports/pmd gradle-build ./gradlew pmdMain pmdTest