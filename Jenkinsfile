pipeline {
    agent any
    
/* 
Les variables d'environnement utilisées par le Pipeline sont définies à ce niveau
 Certaines variables sont à modifier suivant le cas d'utilisation, comme indiqué: */
environment {

    /* l'url de la registry docker est à définir à ce niveau, si votre registry est différente 
    de celle utilisée ci-dessous, veuillez entrer l'url de votre registry */
    registry= "https://registry.hub.docker.com"

    /* Veuillez indiquer le repertoire de votre registry docker, 
    dans le cas de dockerhub, le repertoire correspond à ce qui suit: {username]/{nom du repository} */
    registryDirectory = "otmanebah/java-gradle-jenkins"

    /* Veuillez indiquer l' ID des Credentials créé sur jenkins de votre docker hub (ou votre registry)*/
    registryCredential = "dockerhub"

    /* Veuillez définir le quality Gate des tests statics souhaité */
    qualityGate = "5"
    
    /* Variable à ne pas renseigner ! */
    dockerImage = ""
   }

    
    stages {
        /* checkout pour recuperer le projet gitlab 'Jenkins-Project-with-Java-app' */
        stage('checkout') {
            agent any 
            steps{
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/OtmaneBah/jenkins-project-with-java-app.git']]])
            }
         }
     /* le stage test qui regroupe les tests statics et dynamiques de la pipeline' */
    stage('Tests'){
    parallel {
        /* le Linter du Dockerfile de l'image openjdk' */
         stage('dockerfile linter test'){
             agent {
                 dockerfile {
                     dir './Docker_hadolint'
                     filename 'Dockerfile'
                 }
             }

                 steps{
                    sh 'cd java-app && hadolint -f json Dockerfile-java-jdk'
         }
        }
         
        /* les tests statiques et dynamique' */
         stage('tests statics & dynamiques ') {
             agent {
                 dockerfile {
                     dir './java-app'
                     filename 'Dockerfile-java-jdk'
                 }
             }
             steps {
                   
                 /* Cette commande permet d'effectuer les tests statiques du code
                 pmdMain permet de tester le code Java source et la commande pmdTest permet de tester le fichier des test de Java' */    
                 sh 'cd java-app/ && ./gradlew pmdMain pmdTest || exit 0' 
                 /* Cette commande recupère le fichier main.xml géneré par les tests statiques et affiche le rapport de resultat sur Jenkins */
                 recordIssues healthy: 1, tools: [pmdParser(pattern: 'java-app/build/reports/pmd/main.xml')], 
                 unhealthy: qualityGate

                 /* Cette commande effectue les tests unitaires du code de l'application*/
                 sh 'cd java-app/ && ./gradlew test || exit 0' 
                 
                 /* Cette commande recupère les fichiers .xml géneré par les tests unitaires et affiche le rapport de resultat sur Jenkins*/
                 junit 'java-app/build/test-results/test/*.xml'
                                       
                 /* Cette commande genère le rapport de couverture de code en tests unitaires*/
                 /* on peut également configurer les seuils de couverture de code souhaités */
                 jacoco buildOverBuild: true, deltaBranchCoverage: '30', deltaClassCoverage: '30', deltaComplexityCoverage: '30', deltaInstructionCoverage: '30', deltaLineCoverage: '30', deltaMethodCoverage: '30',
                 maximumBranchCoverage: '90', maximumClassCoverage: '90', maximumComplexityCoverage: '90', maximumInstructionCoverage: '90', maximumLineCoverage: '90', maximumMethodCoverage: '90', minimumBranchCoverage: '50', 
                 minimumClassCoverage: '50', minimumComplexityCoverage: '50', minimumInstructionCoverage: '50', minimumLineCoverage: '50', minimumMethodCoverage: '50'
                       
            }
         }
    }
  }

      /* Ce stage effectue le build de l'application java (génére le jar) une fois les tests passés  */  
      stage('Build') {
             agent {
                dockerfile {
                    dir './java-app'
                    filename 'Dockerfile-java-jdk'
                }
            }
        steps{
            sh 'cd java-app/  && ./gradlew build -x test'
            }

        }

      /* Ce stage effectue le build de l'image docker de l'application */  
      stage('Build image'){
        agent any
        steps{
            script{
              dockerImage = docker.build("${registryDirectory}:${env.BUILD_ID}", "-f ./java-app/Dockerfile-jar .")
            }
        }
      }
    /* Ce stage effectue le push de l'image docker vers la registry */  
      stage('push image'){
          agent any
          steps{
              script{
                 docker.withRegistry( "${registry}", registryCredential ) {dockerImage.push()}
              }
          }
      }

    }

post {
// effectue le clean du repo après la fin de tous les stages
        always {
            cleanWs()      
        }
    }
}


